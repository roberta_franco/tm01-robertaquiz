﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Quiz
{
    public string texto;
}

public class ClassResposta : Quiz
{
    public bool ecorreta;
    
    public ClassResposta(string a, bool b)
    {
        this.texto = a;
        this.ecorreta = b;
    }
}

public class ClassPergunta : Quiz
{ // CLASS PERGUNTAS
    public int pontos;
    public ClassResposta[] respostas;

    // METODO É PARA CRIAR AS PERGUNTAS
    public ClassPergunta(string t, int p)
    {
        this.texto = t;
        this.pontos = p;
    }

    // METODO PARA ADD RESPOSTA
    public void AddResposta(string a, bool b)
    {
        if (respostas == null)
        {
            respostas = new ClassResposta[4];
        }

        for (int i = 0; i < respostas.Length; i++)
        {
            if (respostas[i] == null)
            {
                respostas[i] = new ClassResposta(a, b);
                break;
            }
        }
    }

    // LISTA ou EXIBIR as respostas
    public void ListarRespostas()
    {
        if (respostas != null && respostas.Length > 0)
        {
            foreach (ClassResposta r in respostas)
            {
                Debug.Log(r);
            }
        }
    }

    // Checar resposta correta
    public bool ChecarRespostaCorreta(int pos)
    {
        if (respostas != null && respostas.Length > pos && pos >= 0)
        {
            return respostas[pos].ecorreta;
        }
        else
        {
            return false;
        }
    }

} // FIM CLASS PERGUNTAS


// CLASS LÓGICA - PRINCIPAL
public class Logica : MonoBehaviour
{ // INICIO CLASS LOGICA

    public Text tituloPergunta;
    public Button[] respostaBtn = new Button[4];
    public List<ClassPergunta> PerguntasLista = new List<ClassPergunta>();

    private ClassPergunta perguntaAtual;

    public GameObject PainelCorreto;
    public GameObject PainelErrado;
    public GameObject PainelFinal;
    private int contagemPerguntas = 0;
    private float tempo;
    private bool carregarProximaPergunta;
    private int pontos = 0;


    void Start()
    { // METODO START

        ClassPergunta p1 = new ClassPergunta("1. Quais equipamentos são necessários para um praticante de Boxe?", 1);
        p1.AddResposta("Luvas sem dedos e protetor bucal", false);
        p1.AddResposta("Luvas e protetor bucal", false);
        p1.AddResposta("Luvas sem dedos, faixas e protetor bucal", false);
        p1.AddResposta("Luvas, faixas e protetor bucal", true);

        ClassPergunta p2 = new ClassPergunta("2. Pelas regras, uma luta de Boxe deve ser composta por quantos rounds?", 1);
        p2.AddResposta("10 rounds", false);
        p2.AddResposta("11 rounds", false);
        p2.AddResposta("12 rounds", true);
        p2.AddResposta("13 rounds", false);

        ClassPergunta p3 = new ClassPergunta("3. Quais destes golpes não faz parte dos movimentos do Boxe?", 1);
        p3.AddResposta("Uppercut", false);
        p3.AddResposta("Jab", false);
        p3.AddResposta("Direito", true);
        p3.AddResposta("Cruzado", false);

        ClassPergunta p4 = new ClassPergunta("4. Quantos minutos dura 1 round? ", 1);
        p4.AddResposta("2 minutos", false);
        p4.AddResposta("3 minutos", true);
        p4.AddResposta("5 minutos", false);
        p4.AddResposta("10 minutos", false);

        ClassPergunta p5 = new ClassPergunta("5. Como se chama a posição de defesa de um lutador?", 2);
        p5.AddResposta("Guarda fechada", true);
        p5.AddResposta("Base fechada", false);
        p5.AddResposta("Guarda alta", false);
        p5.AddResposta("Base alta", false);

        ClassPergunta p6 = new ClassPergunta("6. Qual destas combinações pode ser definida como golpes em linha? ", 2);
        p6.AddResposta("Uppercut + Cruzado", false);
        p6.AddResposta("Jab + Direto", true);
        p6.AddResposta("Uppercut + Jab", false);
        p6.AddResposta("Cruzado + Direto", false);

        ClassPergunta p7 = new ClassPergunta("7. Um canhoto aplicaria um jab utilizando qual mão?", 3);
        p7.AddResposta("Mão direita", true);
        p7.AddResposta("Mão esquerda", false);
        p7.AddResposta("Ambas as mãos", false);
        p7.AddResposta("Ele só aplica diretos", false);

        ClassPergunta p8 = new ClassPergunta("8. O movimento de esquiva utilizado para evitar um cruzado é conhecido como:", 3);
        p8.AddResposta("Evasiva", false);
        p8.AddResposta("Balança", false);
        p8.AddResposta("Pêndulo", true);
        p8.AddResposta("Gangorra", false);

        ClassPergunta p9 = new ClassPergunta("9. No Boxe, o que significa “fazer sombra”?", 3);
        p9.AddResposta("Repetir uma sequência contra um oponente", false);
        p9.AddResposta("Treinar uma sequência sozinho", true);
        p9.AddResposta("Imitar a sequência de golpes do oponente", false);
        p9.AddResposta("Não permitir que o oponente se afaste", false);

        ClassPergunta p10 = new ClassPergunta("10. Em uma luta, o que seria uma janela?", 3);
        p10.AddResposta("Identificar partes do oponente que estão fora da guarda", false);
        p10.AddResposta("Fintas que fazem o oponente abrir a guarda", false);
        p10.AddResposta("Espaço na guarda, por onde o lutador vê o oponente", true);
        p10.AddResposta("Movimentações rápidas ao redor do oponente", false);



        PerguntasLista.Add(p1);
        PerguntasLista.Add(p2);
        PerguntasLista.Add(p3);
        PerguntasLista.Add(p4);
        PerguntasLista.Add(p5);
        PerguntasLista.Add(p6);
        PerguntasLista.Add(p7);
        PerguntasLista.Add(p8);
        PerguntasLista.Add(p9);
        PerguntasLista.Add(p10);

        perguntaAtual = p1;

        ExibirPerguntasNoQuiz();


    } // FIM METODO START

    void Update()
    {
        if (carregarProximaPergunta == true)
        {
            tempo += Time.deltaTime;
            if (tempo > 2.5)
            {
                tempo = 0;
                carregarProximaPergunta = false;
                ProximaPergunta();
            }
        }
    }


    private void ExibirPerguntasNoQuiz()
    {
        tituloPergunta.text = perguntaAtual.texto;
        respostaBtn[0].GetComponentInChildren<Text>().text = perguntaAtual.respostas[0].texto;
        respostaBtn[1].GetComponentInChildren<Text>().text = perguntaAtual.respostas[1].texto;
        respostaBtn[2].GetComponentInChildren<Text>().text = perguntaAtual.respostas[2].texto;
        respostaBtn[3].GetComponentInChildren<Text>().text = perguntaAtual.respostas[3].texto;
    }

    public void ProximaPergunta()
    {
        contagemPerguntas++;
        if (contagemPerguntas < PerguntasLista.Count)
        {
            perguntaAtual = PerguntasLista[contagemPerguntas];
            ExibirPerguntasNoQuiz();
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
        }
        else
        {
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
            PainelFinal.SetActive(true);
            PainelFinal.transform.GetComponentInChildren<Text>().text = pontos.ToString();
        }
    }

    public void ClickResposta(int pos)
    {
        if (perguntaAtual.respostas[pos].ecorreta)
        {
            //Debug.Log("Resposta certa!!!");
            PainelCorreto.SetActive(true);
            pontos += perguntaAtual.pontos;
                
        }
        else
        {
            //Debug.Log("Resposta errada!!!");
            PainelErrado.SetActive(true);
        }

        carregarProximaPergunta = true;
    }

    public void IniciarQuiz()
    {
        SceneManager.LoadScene("Quiz");
    }

    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");
    }

    public void Credito()
    {
        SceneManager.LoadScene("Credito");
    }

    public void SairJogo()
    {
        Application.Quit();
    }




}// FIM CLASS LOGICA
